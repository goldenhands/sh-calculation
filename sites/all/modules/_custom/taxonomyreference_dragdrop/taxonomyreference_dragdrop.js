jQuery(document).ready(function($) {
  var $avail = $(".taxonomyreference-dragdrop-available");
  var $select = $(".taxonomyreference-dragdrop-selected");
  
  $avail.sortable({
    connectWith: "ul.taxonomyreference-dragdrop"
  });

  $select.sortable({
    connectWith: "ul.taxonomyreference-dragdrop",
    update: taxonomyreference_dragdrop_update
  });
});

function taxonomyreference_dragdrop_update(event, ui) {
  var items = [];
  jQuery(".taxonomyreference-dragdrop-selected li").each(function(index) {
    items.push(jQuery(this).attr('data-id'));
  });
  
  jQuery("input.taxonomyreference-dragdrop-values").val(items.join(','));
}
