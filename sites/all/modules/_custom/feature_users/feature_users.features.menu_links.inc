<?php
/**
 * @file
 * feature_users.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_users_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-smet-menu:house_projects
  $menu_links['menu-smet-menu:house_projects'] = array(
    'menu_name' => 'menu-smet-menu',
    'link_path' => 'house_projects',
    'router_path' => 'house_projects',
    'link_title' => 'Проекты домов',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Проекты домов');


  return $menu_links;
}
