<?php
/**
 * @file
 * feature_users.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function feature_users_user_default_roles() {
  $roles = array();

  // Exported role: Менеджер.
  $roles['Менеджер'] = array(
    'name' => 'Менеджер',
    'weight' => '3',
  );

  // Exported role: Сметчик.
  $roles['Сметчик'] = array(
    'name' => 'Сметчик',
    'weight' => '4',
  );

  return $roles;
}
