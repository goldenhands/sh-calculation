<?php
/**
 * @file
 * feature_users.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_users_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_style';
  $strongarm->value = 'user_picture';
  $export['user_picture_style'] = $strongarm;

  return $export;
}
