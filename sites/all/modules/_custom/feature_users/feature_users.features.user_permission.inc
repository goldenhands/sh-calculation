<?php
/**
 * @file
 * feature_users.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_users_user_default_permissions() {
  $permissions = array();

  // Exported permission: access PDF version.
  $permissions['access PDF version'] = array(
    'name' => 'access PDF version',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
    ),
    'module' => 'print_pdf',
  );

  // Exported permission: access print.
  $permissions['access print'] = array(
    'name' => 'access print',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
      2 => 'Сметчик',
    ),
    'module' => 'print',
  );

  // Exported permission: administer project system.
  $permissions['administer project system'] = array(
    'name' => 'administer project system',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'project_system',
  );

  // Exported permission: administer taxonomy.
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: administer taxonomy display.
  $permissions['administer taxonomy display'] = array(
    'name' => 'administer taxonomy display',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'taxonomy_display',
  );

  // Exported permission: create house_project content.
  $permissions['create house_project content'] = array(
    'name' => 'create house_project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
      2 => 'Сметчик',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any house_project content.
  $permissions['delete any house_project content'] = array(
    'name' => 'delete any house_project content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own house_project content.
  $permissions['delete own house_project content'] = array(
    'name' => 'delete own house_project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
      2 => 'Сметчик',
    ),
    'module' => 'node',
  );

  // Exported permission: delete terms in 1.
  $permissions['delete terms in 1'] = array(
    'name' => 'delete terms in 1',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 2.
  $permissions['delete terms in 2'] = array(
    'name' => 'delete terms in 2',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 3.
  $permissions['delete terms in 3'] = array(
    'name' => 'delete terms in 3',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit  project system.
  $permissions['edit  project system'] = array(
    'name' => 'edit  project system',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'project_system',
  );

  // Exported permission: edit any house_project content.
  $permissions['edit any house_project content'] = array(
    'name' => 'edit any house_project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
      2 => 'Сметчик',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own house_project content.
  $permissions['edit own house_project content'] = array(
    'name' => 'edit own house_project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
      2 => 'Сметчик',
    ),
    'module' => 'node',
  );

  // Exported permission: edit terms in 1.
  $permissions['edit terms in 1'] = array(
    'name' => 'edit terms in 1',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 2.
  $permissions['edit terms in 2'] = array(
    'name' => 'edit terms in 2',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 3.
  $permissions['edit terms in 3'] = array(
    'name' => 'edit terms in 3',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: export to xls.
  $permissions['export to xls'] = array(
    'name' => 'export to xls',
    'roles' => array(
      0 => 'administrator',
      1 => 'Сметчик',
    ),
    'module' => 'project_system',
  );

  // Exported permission: view  project system.
  $permissions['view  project system'] = array(
    'name' => 'view  project system',
    'roles' => array(
      0 => 'administrator',
      1 => 'Менеджер',
      2 => 'Сметчик',
    ),
    'module' => 'project_system',
  );

  return $permissions;
}
