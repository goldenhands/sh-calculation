<?php
/**
 * @file
 * feature_users.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_users_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-manager-menu
  $menus['menu-manager-menu'] = array(
    'menu_name' => 'menu-manager-menu',
    'title' => 'Меню менеджера',
    'description' => 'Меню для роли менеджер',
  );
  // Exported menu: menu-smet-menu
  $menus['menu-smet-menu'] = array(
    'menu_name' => 'menu-smet-menu',
    'title' => 'Меню сметчика',
    'description' => 'Меню для сметчиков',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Меню для роли менеджер');
  t('Меню для сметчиков');
  t('Меню менеджера');
  t('Меню сметчика');


  return $menus;
}
