<?php

function project_system_xls_sheet_settings(){
  $res = db_select('project_system_xls_sheet', 'psxs')
    ->fields('psxs', array())
    ->orderBy('weight')
    ->execute();

  $rows = array();
  $header = array('ID', 'Sheet Name', 'Sheet Caption', 'weight', 'actions');
  foreach ($res as $sheet){
    $row = array();
    $row['id'] = $sheet->sheet_id;
    $row['sheet_name'] = $sheet->name;
    $row['sheet_caption'] = $sheet->caption;
    $row['weight'] = $sheet->weight;
    $row['actions'] = l(t('Delete'), 'project_system/xls_sheet/' . $sheet->sheet_id . '/delete') . '&nbsp;&nbsp;&nbsp;' .
      l(t('Edit'),   'project_system/xls_sheet/' . $sheet->sheet_id . '/edit') ;
    $rows[] = $row;
  }

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('You don\'t have xls sheets for export.')
  ));

  $output .= '<br><h3>' . t('Add XLS Sheet') . '</h3>';
  $form = drupal_get_form('project_system_xls_sheet_add_form', array());
  $output .= drupal_render($form);
  return $output;
}

function project_system_xls_sheet_add_form($form, &$form_state){
  $form = array();
  $form['sheet_name'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#title' => t('Sheet Name'),
  );

  $form['sheet_caption'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#title' => t('Sheet Caption'),
  );

  $form['weight'] = array(
    '#type' => 'textfield',
    '#default_value' => '0',
    '#title' => t('Weight'),
  );

  $form['add_sheet'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

function project_system_xls_sheet_add_form_submit($form, &$form_state){
  db_insert('project_system_xls_sheet')
    ->fields(array(
    'name' => $form_state['values']['sheet_name'],
    'caption' => $form_state['values']['sheet_caption'],
    'weight' => $form_state['values']['weight'],
  ))
    ->execute();

  return;
}

function project_system_delete_xls_sheet_form($sheet_id){
  return confirm_form(
    array(
      'sheet_id' => array(
        '#type' => 'value',
        '#value' => $sheet_id,
      ),
    ),
    t('Are you sure you want to remove xls sheet with id = %sheet_id ?', array(
      '%sheet_id' => $sheet_id,
    )),
    'admin/config/project-system/xls',
    t('This action cannot be undone.'),
    t('Remove sheet'),
    t('Cancel')
  );
}

function project_system_delete_xls_sheet_form_submit($form, $form_state){
  $deleted = db_delete('project_system_xls_sheet')
    ->condition('sheet_id', $form_state['values']['sheet_id'])
    ->execute();

  drupal_set_message(t("Sheet %sheet_id successfully deleted.", array(
    '%sheet_id' => $form_state['values']['sheet_id'],
  )));

  drupal_goto('admin/config/project-system/xls');
}

function project_system_edit_xls_sheet_form($form, $form_state, $sheet_id){
  //retrieve existing sections
  $structure = db_select('project_system_xls_sheet', 'psxs')
    ->condition('sheet_id', $sheet_id)
    ->fields('psxs', array())
    ->execute()
    ->fetchAssoc();

  $form = array();
  $form['sheet_id'] = array(
    '#type' => 'value',
    '#value' => $structure['sheet_id'],
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#default_value' => $structure['name'],
    '#title' => t('Sheet Name'),
  );

  $form['caption'] = array(
    '#type' => 'textfield',
    '#default_value' => $structure['caption'],
    '#title' => t('Sheet Caption'),
  );

  $form['weight'] = array(
    '#type' => 'textfield',
    '#default_value' => $structure['weight'],
    '#title' => t('Weight'),
  );

  $form['edit_sheet'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['back'] = array(
    '#markup' => l(t('Back to xls sheets list'), 'admin/config/project-system/xls'),
  );

  return $form;
}

function project_system_edit_xls_sheet_form_submit($form, $form_state){
  db_update('project_system_xls_sheet')
    ->fields(array(
    'name' => $form_state['values']['name'],
    'caption' => $form_state['values']['caption'],
    'weight' => $form_state['values']['weight'],
  ))
    ->condition('sheet_id', $form_state['values']['sheet_id'])
    ->execute();

  drupal_goto('admin/config/project-system/xls');
}

function project_system_xls_sheet_sections_settings(){
  $res = db_select('project_system_xls_sheet_sections', 'psxss')
    ->fields('psxss', array())
    ->orderBy('sheet_id')
    ->orderBy('weight')
    ->execute();

  $rows = array();
  $header = array('Sheet Name', 'Section ID', 'Alternative Section ID', 'weight', 'actions');
  foreach ($res as $sheet_section){
    $row = array();
    $sheet = db_select('project_system_xls_sheet', 'psxs')
      ->fields('psxs', array('name'))
      ->condition('sheet_id', $sheet_section->sheet_id)
      ->execute()
      ->fetchAssoc();
    $row['sheet_name'] = $sheet['name'];
    $section_term = taxonomy_term_load($sheet_section->section_id);
    $row['section_id'] = $section_term->name;
    if ($sheet_section->alter_section_id != 0){
      $alter_section_term = taxonomy_term_load($sheet_section->alter_section_id);
      $row['alter_section_id'] = $alter_section_term->name;
    }
    else $row['alter_section_id'] = t('None');

    $row['weight'] = $sheet_section->weight;
    $row['actions'] = l(t('Delete'), 'project_system/xls_sheet_section/' . $sheet_section->sheet_section_id . '/delete') . '&nbsp;&nbsp;&nbsp;' .
      l(t('Edit'),   'project_system/xls_sheet_section/' . $sheet_section->sheet_section_id . '/edit') ;
    $rows[] = $row;
  }

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('You don\'t have xls sheets sections.')
  ));

  $output .= '<br><h3>' . t('Add XLS Sheet Section') . '</h3>';
  $form = drupal_get_form('project_system_xls_sheet_section_add_form', array());
  $output .= drupal_render($form);
  return $output;
}

function project_system_xls_sheet_section_add_form($form, &$form_state){
  $form = array();
  $res = db_select('project_system_xls_sheet', 'psxs')
    ->fields('psxs', array())
    ->execute();

  $sheets = array();
  foreach ($res as $sheet) {
    $sheets[$sheet->sheet_id] = $sheet->name;
  }

  $terms = taxonomy_get_tree(3);
  $sections = array(0 => '- None -');
  foreach ($terms as $term){
    $sections[$term->tid] = $term->name;
  }

  $form['sheet_id'] = array(
    '#type' => 'select',
    '#default_value' => '',
    '#options' => $sheets,
    '#title' => t('Sheet Name'),
  );

  $form['section_id'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $sections,
    '#title' => t('Section'),
  );

  $form['alter_section_id'] = array(
    '#type' => 'select',
    '#default_value' => 0,
    '#options' => $sections,
    '#title' => t('Alter Section'),
  );

  $form['weight'] = array(
    '#type' => 'textfield',
    '#default_value' => '0',
    '#title' => t('Weight'),
  );

  $form['add_section'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

function project_system_xls_sheet_section_add_form_submit($form, &$form_state){
  db_insert('project_system_xls_sheet_sections')
    ->fields(array(
    'sheet_id' => $form_state['values']['sheet_id'],
    'section_id' => $form_state['values']['section_id'],
    'alter_section_id' => $form_state['values']['alter_section_id'],
    'weight' => $form_state['values']['weight'],
  ))
    ->execute();

  return;
}

function project_system_delete_xls_sheet_section_form($sheet_section_id){
  return confirm_form(
    array(
      'sheet_section_id' => array(
        '#type' => 'value',
        '#value' => $sheet_section_id,
      ),
    ),
    t('Are you sure you want to remove xls sheet section?'),
    'admin/config/project-system/xls/sheet_sections',
    t('This action cannot be undone.'),
    t('Remove sheet section'),
    t('Cancel')
  );
}

function project_system_delete_xls_sheet_section_form_submit($form, $form_state){
  $deleted = db_delete('project_system_xls_sheet_sections')
    ->condition('sheet_section_id', $form_state['values']['sheet_section_id'])
    ->execute();

  drupal_set_message(t("Sheet section successfully deleted."));

  drupal_goto('admin/config/project-system/xls/sheet_sections');
}

function project_system_edit_xls_sheet_section_form($form, $form_state, $sheet_section_id){
  //retrieve existing sections
  $sheet_section = db_select('project_system_xls_sheet_sections', 'psxss')
    ->condition('sheet_section_id', $sheet_section_id)
    ->fields('psxss', array())
    ->execute()
    ->fetchAssoc();

  $res = db_select('project_system_xls_sheet', 'psxs')
    ->fields('psxs', array())
    ->execute();

  $sheets = array();
  foreach ($res as $sheet) {
    $sheets[$sheet->sheet_id] = $sheet->name;
  }

  $terms = taxonomy_get_tree(3);
  $sections = array(0 => '- None -');
  foreach ($terms as $term){
    $sections[$term->tid] = $term->name;
  }

  $form = array();
  $form['sheet_section_id'] = array(
    '#type' => 'value',
    '#value' => $sheet_section['sheet_section_id'],
  );

  $form['sheet_id'] = array(
    '#type' => 'select',
    '#options' => $sheets,
    '#default_value' => $sheet_section['sheet_id'],
  );

  $form['section_id'] = array(
    '#type' => 'select',
    '#options' => $sections,
    '#default_value' => $sheet_section['section_id'],
    '#title' => t('Section ID'),
  );

  $form['alter_section_id'] = array(
    '#type' => 'select',
    '#options' => $sections,
    '#default_value' => $sheet_section['alter_section_id'],
    '#title' => t('Alter Section ID'),
  );

  $form['weight'] = array(
    '#type' => 'textfield',
    '#default_value' => $sheet_section['weight'],
    '#title' => t('Weight'),
  );

  $form['edit_sheet_section'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['back'] = array(
    '#markup' => l(t('Back to xls sheets sections list'), 'admin/config/project-system/xls/sheet_sections'),
  );

  return $form;
}

function project_system_edit_xls_sheet_section_form_submit($form, $form_state){
  db_update('project_system_xls_sheet_sections')
    ->fields(array(
    'sheet_id' => $form_state['values']['sheet_id'],
    'section_id' => $form_state['values']['section_id'],
    'alter_section_id' => $form_state['values']['alter_section_id'],
    'weight' => $form_state['values']['weight'],
  ))
    ->condition('sheet_section_id', $form_state['values']['sheet_section_id'])
    ->execute();

  drupal_goto('admin/config/project-system/xls/sheet_sections');
}

function project_system_export_xls($node){
  $time = time();
  module_load_include('inc', 'phpexcel', 'phpexcel.api');
  $data = array();
  $headers = array();

  $res = db_select('project_system_xls_sheet', 'psxs')
    ->fields('psxs', array())
    ->execute();

  $sheets = array();
  $sheets_captions = array();
  foreach ($res as $sheet) {
    $sheets[$sheet->sheet_id] = $sheet->name;
    $sheets_captions[$sheet->sheet_id] = $sheet->caption;
  }

  $date_time = date_create('@' . time());
  $vars = array('@current-date' => date_format($date_time, 'd.m.Y'));
  $h = (isset($node->field_xls_header['und'][0]['value'])) ? t($node->field_xls_header['und'][0]['value'], $vars) : '123';
  $header = array($h, '1', '2', '3', '4', '5', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
  foreach ($sheets as $sheet_id => $sheet_name){
    $parameter_table = _project_system_get_project_parameters_xls($node->nid, $sheet_id);
    //$headers[$sheet_name] = $parameter_table['headers'];
    $headers[$sheet_name] = $header;
    $addditional = array($parameter_table['headers'], array($sheets_captions[$sheet_id]));
    $data[$sheet_name] = array_merge($addditional, $parameter_table['rows']);
  }

  // Store the file in sites/default/files
  $dir = drupal_realpath(file_default_scheme() . '://') . '/xls';
  $filename =  'project' . $node->nid . '.xls';
  $path = $dir . '/' . $filename;
  if (!file_exists($path)) {
    $fp = fopen($path, "w");
    fclose ($fp);
  }
  // Use the .xls format
  $options = array('format' => 'xls', 'ignore_headers' => FALSE);
  if (phpexcel_export($headers, $data, $path, $options)) {
    global $user;
    watchdog('debug', 'XLS generated as %time seconds. User %user. Node %node', array('%time' => time()-$time, '%user' => $user->name, '%node' => $node->title));
    drupal_goto(variable_get('file-public-path', 'sites/default/files') . '/xls/' . $filename);
  }
  else {
    drupal_set_message(t("Oops ! An error occured !"), 'error');
    return t("File not exported");
  }
}

function _project_system_get_project_parameters_xls($nid, $sheet_id){
  $rows = array();
  $headers = array(t('Number'),
    t('Name'),
    t('Measurement'),
    t('Price'),
    t('Count'),
    t('Amount'));

  $res = db_select('project_system_xls_sheet_sections', 'psxss')
    ->condition('sheet_id', $sheet_id)
    ->fields('psxss', array())
    ->orderBy('weight')
    ->execute();

  $sheet_sections = array();
  foreach ($res as $sheet_section) {
    $sheet_sections[] = $sheet_section->section_id;
  }

  //если нет секций возвращаем пустые массивы
  if (count($sheet_sections) == 0){
    return array('headers' => array(), 'rows' => array());
  }

  $res = db_select('project_parameters', 'pp')
    ->fields('pp', array())
    ->condition('nid', $nid)
    ->condition('p_section_id', $sheet_sections)
    ->orderBy('p_section_id')
    ->orderBy('weight')
    ->execute();

  $amount_number =5;
  $alter_section_amount_number =12;
  $alter_section2_amount_number =19;
  $current_section = 0;
  $section_amount = 0;
  global $alter_section_amount;
  $alter_section_amount = 0;
  global $alter_section2_amount;
  $alter_section2_amount = 0;
  $all_amount = 0;
  $i=1;
  foreach ($res as $parameter) {
    if ($current_section <> $parameter->p_section_id){
      if ($current_section !== 0){
        $row = array();
        $row[] = $i;
        $row[] = t('Total: ');
        $row[$amount_number] = $section_amount;
        if ($alter_section_amount !== 0){
          $row[7] = $i;
          $row[8] = t('Total: ');
          $row[$alter_section_amount_number] = $alter_section_amount;
          $alter_section_amount = 0;
        }
        if ($alter_section2_amount !== 0){
          $row[14] = $i;
          $row[15] = t('Total: ');
          $row[$alter_section2_amount_number] = $alter_section2_amount;
          $alter_section2_amount = 0;
        }
        $rows[] = $row;
        $all_amount += $section_amount;
        $section_amount = 0;
        $i=1;
      }
      $section_term = taxonomy_term_load($parameter->p_section_id);

      $term_row = array(0 => $section_term->name);

      $alter_section_id = _check_alter_section_xls($sheet_id, $parameter->p_section_id);
      if ($alter_section_id !== FALSE){
        $alter_section_term = taxonomy_term_load($alter_section_id);
        $term_row[7] =  $alter_section_term->name;
        if ($alter_section_id == 112){
          $alter_section2_term = taxonomy_term_load(114);
          $term_row[14] =  $alter_section2_term->name;
        }
      }
      $rows[] = $term_row;
      $current_section = $parameter->p_section_id;
    }
    $parameter_term = taxonomy_term_load($parameter->p_subsection_id);
    $measurment_term = taxonomy_term_load($parameter_term->field_measurment['und'][0]['tid']);
    $row = array();
    $row[] = $i;
    $row[] = $parameter_term->name;
    $row[] = $measurment_term->name;
    $row[] = $parameter_term->field_price['und'][0]['value'];
    $row[] = $parameter->p_subsection_count;
    $amount = $parameter_term->field_price['und'][0]['value'] * $parameter->p_subsection_count;
    $row[] = $amount;
    $section_amount += $amount;
    $alter_section_id = _check_alter_section_xls($sheet_id, $parameter->p_section_id);
    if ($alter_section_id !== FALSE){
      _add_alter_section_xls($alter_section_id, $row, $i, $nid);
    }


    $rows[] = $row;
    $i++;
  }

  //last subtotal
  $row = array();
  $row[] = $i;
  $row[] =  t('Total: ');
  $row[$amount_number] =  $section_amount;
  $all_amount += $section_amount;
  $rows[] = $row;

  //all project total
  $row = array();
  $row[] = '';
  $row[] = t('Project total: ');
  $row[$amount_number] = $all_amount;
  $rows[] = $row;

  return array('headers' => $headers, 'rows' =>$rows);
}

function _check_alter_section_xls($sheet_id, $section_id){
  $alter_section_id = db_select('project_system_xls_sheet_sections', 'psxss')
    ->condition('sheet_id', $sheet_id)
    ->condition('section_id', $section_id)
    ->condition('alter_section_id', 0, '<>')
    ->fields('psxss', array('alter_section_id'))
    ->execute()
    ->fetchField();

  return $alter_section_id;

}

function _add_alter_section_xls($alter_section_id, &$row, $number, $nid){
  global $alter_section_amount;
  global $alter_section2_amount;
  $res = db_select('project_parameters', 'pp')
    ->fields('pp', array())
    ->condition('nid', $nid)
    ->condition('p_section_id', $alter_section_id)
    ->condition('weight', $number-1)
    ->execute();

  foreach ($res as $parameter) {
    $parameter_term = taxonomy_term_load($parameter->p_subsection_id);
    $measurment_term = taxonomy_term_load($parameter_term->field_measurment['und'][0]['tid']);
    $row[] = '';
    $row[] = $number;
    $row[] = $parameter_term->name;
    $row[] = $measurment_term->name;
    $row[] = $parameter_term->field_price['und'][0]['value'];
    $row[] = $parameter->p_subsection_count;
    $amount = $parameter_term->field_price['und'][0]['value'] * $parameter->p_subsection_count;
    $row[] = $amount;
    $alter_section_amount += $amount;
    //$section_amount += $amount;
  }

  //хак на коробку - 3ая
  if ($alter_section_id == 112){
    $alter_section_id2 = 114;
    $res = db_select('project_parameters', 'pp')
      ->fields('pp', array())
      ->condition('nid', $nid)
      ->condition('p_section_id', $alter_section_id2)
      ->condition('weight', $number-1)
      ->execute();

    foreach ($res as $parameter) {
      $parameter_term = taxonomy_term_load($parameter->p_subsection_id);
      $measurment_term = taxonomy_term_load($parameter_term->field_measurment['und'][0]['tid']);
      $row[] = '';
      $row[] = $number;
      $row[] = $parameter_term->name;
      $row[] = $measurment_term->name;
      $row[] = $parameter_term->field_price['und'][0]['value'];
      $row[] = $parameter->p_subsection_count;
      $amount = $parameter_term->field_price['und'][0]['value'] * $parameter->p_subsection_count;
      $row[] = $amount;
      $alter_section2_amount += $amount;
      //$section_amount += $amount;
    }
  }
}