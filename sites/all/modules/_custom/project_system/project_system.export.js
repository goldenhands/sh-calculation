jQuery(document).ready(function($) {
    //changed fundament (1)
   $('.form-item-section-1 .form-radio'). change(function(){
       var checked_value = ($(this).attr("value"));
       //если выбрана плита как фундамент
       if (checked_value == '48_54') {
           // 2.1 disable
           $('input#edit-section-8-12').attr("disabled","disabled");
           $('input#edit-section-8-12').removeAttr("checked");
           // 3.2 disable
           $('input#edit-section-12-70').attr("disabled","disabled");
           $('input#edit-section-12-70').removeAttr("checked");

           //enable 2.2
           $('input#edit-section-8-446').removeAttr("disabled");
           //enable 3.1
           $('input#edit-section-12-447').removeAttr("disabled");

           //включаем свой подвенечный оклад
           $('input#edit-section-8-empty-choice-8').attr("checked","checked");
       }
       else{
           // 2.1 enabled
           $('input#edit-section-8-12').removeAttr("disabled");
           // 3.2 enabled
           $('input#edit-section-12-70').removeAttr("disabled");

           //disabled 2.2
           $('input#edit-section-8-446').attr("disabled","disabled");
           $('input#edit-section-8-446').removeAttr("checked");
           //disabled 3.1
           $('input#edit-section-12-447').attr("disabled","disabled");
           $('input#edit-section-12-447').removeAttr("checked");

           //включаем свой подвенечный оклад
           $('input#edit-section-8-empty-choice-8').attr("checked","checked");
       }
   });

   //changed korobka (4)
   $('.form-item-section-25 .form-radio'). change(function(){
       var checked_value = ($(this).attr("value"));
       //если выбрана плита как фундамент
       if (checked_value == '109' || checked_value == '112') {
           // 9.3 disable
           $('input#edit-section-45-295').attr("disabled","disabled");
           $('input#edit-section-45-295').removeAttr("checked");
           // 9.4 disable
           $('input#edit-section-45-296').attr("disabled","disabled");
           $('input#edit-section-45-296').removeAttr("checked");

           //enable 9.1
           $('input#edit-section-45-293').removeAttr("disabled");
           //enable 9.2
           $('input#edit-section-45-294').removeAttr("disabled");
       }
       else if (checked_value == '114'){
           // 9.1 disable
           $('input#edit-section-45-293').attr("disabled","disabled");
           $('input#edit-section-45-293').removeAttr("checked");
           // 9.2 disable
           $('input#edit-section-45-294').attr("disabled","disabled");
           $('input#edit-section-45-294').removeAttr("checked");

           //enable 9.3
           $('input#edit-section-45-295').removeAttr("disabled");
           //enable 9.4
           $('input#edit-section-45-296').removeAttr("disabled");
       }
       else if (checked_value == 'empty_choice_25'){
           //enable 9.1
           $('input#edit-section-45-293').removeAttr("disabled");
           //enable 9.2
           $('input#edit-section-45-294').removeAttr("disabled");
           //enable 9.3
           $('input#edit-section-45-295').removeAttr("disabled");
           //enable 9.4
           $('input#edit-section-45-296').removeAttr("disabled");
       }

   });

    //changed strop system (5)
    $('.form-item-section-79 .form-radio'). change(function(){
        var checked_value = ($(this).attr("value"));
        //если 5.1
        if (checked_value == '149') {
            // 6.2 disable
            $('input#edit-section-30-214').attr("disabled","disabled");
            $('input#edit-section-30-214').removeAttr("checked");
            // 6.3 disable
            $('input#edit-section-30-245-228').attr("disabled","disabled");
            $('input#edit-section-30-245-228').removeAttr("checked");

            //enable 6.1
            $('input#edit-section-30-183').removeAttr("disabled");
            //check 6.4
            $('input#edit-section-30-empty-choice-30').attr("checked","checked");
        }
        //if 5.2
        else if (checked_value == '158'){
            // 6.1 disable
            $('input#edit-section-30-183').attr("disabled","disabled");
            $('input#edit-section-30-183').removeAttr("checked");
            // 6.3 disable
            $('input#edit-section-30-245-228').attr("disabled","disabled");
            $('input#edit-section-30-245-228').removeAttr("checked");

            //enable 6.2
            $('input#edit-section-30-214').removeAttr("disabled");
            //check 6.4
            $('input#edit-section-30-empty-choice-30').attr("checked","checked");
        }
        //if 5.3
        else if (checked_value == '164'){
            // 6.1 disable
            $('input#edit-section-30-183').attr("disabled","disabled");
            $('input#edit-section-30-183').removeAttr("checked");
            // 6.2 disable
            $('input#edit-section-30-214').attr("disabled","disabled");
            $('input#edit-section-30-214').removeAttr("checked");

            //enable 6.3
            $('input#edit-section-30-245-228').removeAttr("disabled");
            //check 6.4
            $('input#edit-section-30-empty-choice-30').attr("checked","checked");
        }
        //if 5.4
        else if (checked_value == 'empty_choice_79'){
            //enable 6.1
            $('input#edit-section-30-183').removeAttr("disabled");
            //enable 6.2
            $('input#edit-section-30-214').removeAttr("disabled");
            //enable 6.3
            $('input#edit-section-30-441-228').removeAttr("disabled");
        }

    });

    //changed krovlya (6)
    $('.form-item-section-30 .form-radio'). change(function(){
        var checked_value = ($(this).attr("value"));
        //если 6.3
        if (checked_value == '245_228') {
            //checked and disable 7.2
            $('input#edit-section-36-268').attr("checked","checked");
            $('input#edit-section-36-268').attr("disabled","disabled");
       }
       else{
            //unchecked and enable 7.2
            $('input#edit-section-36-268').removeAttr("checked");
            $('input#edit-section-36-268').removeAttr("disabled");
        }
    });

    //click spec offer (11.7)
    $('.form-item-section-69 .form-item-section-69-438 input').change(function(){
        var is_checked = ($(this).attr("checked"));
        //если 11.7
        if (is_checked) {
            //disable 11.1 - 11.6
            $('input#edit-section-69-410').attr("disabled","disabled");
            $('input#edit-section-69-410').removeAttr("checked");
            $('input#edit-section-69-412').attr("disabled","disabled");
            $('input#edit-section-69-412').removeAttr("checked");
            $('input#edit-section-69-416').attr("disabled","disabled");
            $('input#edit-section-69-416').removeAttr("checked");
            $('input#edit-section-69-420').attr("disabled","disabled");
            $('input#edit-section-69-420').removeAttr("checked");
            $('input#edit-section-69-427').attr("disabled","disabled");
            $('input#edit-section-69-427').removeAttr("checked");
            $('input#edit-section-69-428').attr("disabled","disabled");
            $('input#edit-section-69-428').removeAttr("checked");
        }
        else{
            //enable 11.1 - 11.6
            $('input#edit-section-69-410').removeAttr("disabled");
            $('input#edit-section-69-412').removeAttr("disabled");
            $('input#edit-section-69-416').removeAttr("disabled");
            $('input#edit-section-69-420').removeAttr("disabled");
            $('input#edit-section-69-427').removeAttr("disabled");
            $('input#edit-section-69-428').removeAttr("disabled");
        }
    });

    //click 3.9 - check all
    $('.form-item-section-12 .form-item-section-12-empty-choice-12 input').change(function(){
        var is_checked = ($(this).attr("checked"));
        //если 3.9
        if (is_checked) {
            //checked 3.1 - 3.8
            $('input#edit-section-12-447').attr("checked", "checked");
            $('input#edit-section-12-70').attr("checked", "checked");
            $('input#edit-section-12-72').attr("checked", "checked");
            $('input#edit-section-12-73').attr("checked", "checked");
            $('input#edit-section-12-79').attr("checked", "checked");
            $('input#edit-section-12-126').attr("checked", "checked");
            $('input#edit-section-12-133').attr("checked", "checked");
            $('input#edit-section-12-154').attr("checked", "checked");
        }
        else{
            //unchecked 3.1 - 3.8
            $('input#edit-section-12-447').removeAttr("checked");
            $('input#edit-section-12-70').removeAttr("checked");
            $('input#edit-section-12-72').removeAttr("checked");
            $('input#edit-section-12-73').removeAttr("checked");
            $('input#edit-section-12-79').removeAttr("checked");
            $('input#edit-section-12-126').removeAttr("checked");
            $('input#edit-section-12-133').removeAttr("checked");
            $('input#edit-section-12-154').removeAttr("checked");
        }
    });

    //click 3.1 - disable 3.2
    $('.form-item-section-12 .form-item-section-12-447 input').change(function(){
        var is_checked = ($(this).attr("checked"));
        var fundament_plita = $('input#edit-section-1-48-54').attr("checked");
        if (is_checked) {
            //disble 3.2
            $('input#edit-section-12-70').attr("disabled", "disabled");
            $('input#edit-section-12-70').removeAttr("checked");
        }
        else if (!fundament_plita){
            //enable 3.2
            $('input#edit-section-12-70').removeAttr("disabled");
        }
    });

    //click 3.2 - disable 3.1
    $('.form-item-section-12 .form-item-section-12-70 input').change(function(){
        var is_checked = ($(this).attr("checked"));
        var fundament_plita = $('input#edit-section-1-48-54').attr("checked");
        if (is_checked && !fundament_plita) {
            //disble 3.2
            $('input#edit-section-12-447').attr("disabled", "disabled");
            $('input#edit-section-12-447').removeAttr("checked");
        }
        else{
            //enable 3.2
            $('input#edit-section-12-447').removeAttr("disabled");
        }
    });
});